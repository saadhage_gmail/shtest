import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HDFSread {

    public static void main(String[] args) throws IOException
    {
        // obtenir un object représentant HDFS
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);

        // nom du fichier à lire
        Path nomcomplet = new Path("/user/saad/wordcount/input","file02");

        if (! fs.exists(nomcomplet)) {
            System.out.println("Le fichier n'existe pas");
        } else {
		try{
            		// ouvrir et lire le fichier
            		FSDataInputStream inStream = fs.open(nomcomplet);
            // available stream to be read
            while(inStream.available()>0) {
            // reads characters encoded with modified UTF-8
            String k = inStream.readLine();
            // print
            System.out.print(k+" ");
         }

            		inStream.close();
		} catch (Exception e){
                        	System.out.println(e);
			}

        // fermer HDFS
        fs.close();
       }
    }
}
