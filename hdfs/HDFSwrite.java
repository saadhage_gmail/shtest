import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HDFSwrite {

    public static void main(String[] args) throws IOException
    {
        // obtenir un object représentant HDFS
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);

        // nom du fichier à créer
        Path nomcomplet = new Path("apitest.txt");

        if (fs.exists(nomcomplet)) {
            System.out.println("Le fichier existe déjà.");
        } else {

            // création et écriture du fichier
            FSDataOutputStream outStream = fs.create(nomcomplet);
            outStream.writeUTF("Bonjour tout le monde !");
            outStream.close();

        }

        // fermer HDFS
        fs.close();
    }
}
