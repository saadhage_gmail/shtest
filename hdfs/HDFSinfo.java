import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.Path;

public class HDFSinfo {

    public static void main(String[] args) throws IOException
    {
        // obtenir un object représentant HDFS
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);

        // nom du fichier à lire
        Path nomcomplet = new Path("apitest.txt");

        if (! fs.exists(nomcomplet)) {
            System.out.println("Le fichier n'existe pas");
        } else {

            // taille du fichier
            FileStatus infos = fs.getFileStatus(nomcomplet);
            System.out.println(Long.toString(infos.getLen()) + " octets");

            // liste des blocs du fichier
            BlockLocation[] blocks = fs.getFileBlockLocations(infos, 0, infos.getLen());
            System.out.println(blocks.length + " blocs :");
            for (BlockLocation blocloc: blocks) {
                System.out.println("\t" + blocloc.toString());
            }

        }

        // fermer HDFS
        fs.close();
    }
}
