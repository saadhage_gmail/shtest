import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;


/**
 * Cette classe est produite par les Mappers à destination du Reducer
 * Elle représente un triplet des sommes partielles (n, Σx, Σx²) permettant
 * de calculer la variance de n valeurs x
 */
public class Variance implements Writable
{
    /* variables membre */
    private long n;
    private double Sx;
    private double Sx2;

    /** constructeur */
    public Variance()
    {
        n = 0L;
        Sx = 0.0;
        Sx2 = 0.0;
    }

    /** écrit this sur sortie, méthode de l'interface Writable */
    public void write(DataOutput sortie) throws IOException
    {
        sortie.writeLong(n);
        sortie.writeDouble(Sx);
        sortie.writeDouble(Sx2);
    }

    /** lit this à partir de l'entree, méthode de l'interface Writable */
    public void readFields(DataInput entree) throws IOException
    {
        n = entree.readLong();
        Sx = entree.readDouble();
        Sx2 = entree.readDouble();
    }

    /**
     * initialise this à (1, valeur, valeur²)
     * @param valeur à mettre dans this
     */
    public void set(double valeur)
    {
        n = 1L;
        Sx = valeur;
        Sx2 = valeur*valeur;
    }

    /**
     * ajoute autre à this
     * @param autre
     */
    public void add(Variance autre)
    {
        n += autre.n;
        Sx += autre.Sx;
        Sx2 += autre.Sx2;
    }

    /**
     * calcule la moyenne représentée par this
     * @return moyenne des valeurs ajoutées à this
     */
    public double getMoyenne()
    {
        return Sx/n;
    }

    /**
     * calcule la variance représentée par this
     * @return variance des valeurs ajoutées à this
     */
    public double getVariance()
    {
        double Mx = Sx / n;
        double Mx2 = Sx2 / n;
        return Mx2 - Mx*Mx;
    }
}
