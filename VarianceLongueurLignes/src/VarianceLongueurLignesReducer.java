import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


/**
 * Cette classe effectue la réduction de toutes les paires ("V", (n, Σx, Σx²)),
 * calcule la variance et émet une paire ("V", Vx)
 */
public class VarianceLongueurLignesReducer
        extends Reducer<Text, Variance, Text, DoubleWritable>
{
    /* valeur de sortie, la clé est la même qu'en entrée */
    private DoubleWritable valeurS = new DoubleWritable();

    /** traite une liste de paires produites par tous les mappers et combiners */
    @Override
    public void reduce(Text cleI, Iterable<Variance> valeursI, Context context)
            throws IOException, InterruptedException
    {
        // définir la clé de sortie
        Text cleS = cleI;

        // calculer la valeur de sortie
        Variance variance = new Variance();
        for (Variance valeurI : valeursI) {
            variance.add(valeurI);
        }
        valeurS.set( variance.getVariance() );

        // émettre une paire (clé, valeur)
        context.write(cleS, valeurS);
    }
}
