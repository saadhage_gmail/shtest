import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


/**
 * Cette classe émet une paire ("V", (1, x, x²)) par ligne de texte reçue, x=longueur de la ligne
 * @note la clé "V" n'a pas d'importance, il faut juste que ça soit la même pour toutes les paires
 */
public class VarianceLongueurLignesMapper
        extends Mapper<LongWritable, Text, Text, Variance>
{
    /* paire (clé, valeur) émise par ce Mapper */
    private Text cleI = new Text("V");
    private Variance valeurI = new Variance();

    /** traite l'une des lignes du fichier */
    @Override
    public void map(LongWritable cleE, Text valeurE, Context context)
            throws IOException, InterruptedException
    {
        try {
            // calculer la valeur de sortie
            valeurI.set(valeurE.getLength());

            // émettre une paire (clé, valeur)
            context.write(cleI, valeurI);

        } catch (Exception e) {
            // rien, on ignore cet arbre
        }
    }
}
